﻿using s1070361.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace s1070361.Controllers
{
    public class BMIController : Controller
    {
        public ActionResult Index()
        {
            return View(new BMIData());
        }




        [HttpPost]
        public ActionResult Index(BMIData data)
        {
            if (data.height < 50 || data.height > 200) {
                ViewBag.heigthError = "身高請輸入50~200的數值";
            }

            if (data.weight < 30 || data.weight > 150)
            {
                ViewBag.weigthError = "請輸入30~150的數值";
            }


            if (ModelState.IsValid)
            {

                var a = (data.height / 100);
                var s = data.weight / (a * a);
                var level = "";
                if (s < 18.5)
                {
                    level = "體重過輕";
                }
                else if (18.5 <= s && s < 24)
                {
                    level = "正常範圍";
                }
                else if (24 <= s && s < 27)
                {
                    level = "過重";
                }
                else if (27 <= s && s < 30)
                {
                    level = "輕度肥胖";
                }
                else if (30 <= s && s < 35)
                {
                    level = "中度肥胖";
                }
                else if (35 <= s)
                {
                    level = "重度肥胖";
                }
                data.result = s;
                data.level= level;
            }
          
            return View(data);
        }
    }
}